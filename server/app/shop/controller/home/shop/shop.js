const moment = require("moment");
const base = require("./../base");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
        await super.isAppAuth();
        await super.isShopAuth();
    }

    /**
     *
     * @api {get} /shop/home/shop/shop/index 店铺信息
     * @apiDescription 店铺信息
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     *
     * @apiSampleRequest /shop/home/shop/shop/index
     *
     */
    async index() {
        const shopId = this.state.shop.id;
        const shop = await this.model("shop")
            .query(qb => {
                qb.where("id", shopId);
            })
            .fetch();
        this.success(shop);
    }

    /**
     *
     * @api {get} /shop/home/shop/shop/add 新增/修改店铺
     * @apiDescription 新增/修改店铺
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {String} name        店铺名称
     * @apiParam {String} img_url     店铺图片
     * @apiParam {String} info        店铺简介
     * @apiParam {String} tel         店铺电话
     * @apiParam {String} address     店铺地址
     * @apiParam {String} lng         经度
     * @apiParam {String} lat         纬度
     * @apiParam {Number} status      店铺状态（1：开启，0：关闭）
     *
     * @apiSampleRequest /shop/home/shop/shop/index
     *
     */
    async add() {
        const shopId = this.state.shop.id;
        const appId = this.state.app.id;
        const {
            name,
            img_url,
            info,
            tel,
            address,
            lng,
            lat,
            status
        } = this.post;
        const shop = await this.model("shop")
            .forge({
                id: shopId,
                app_id: appId,
                name,
                img_url,
                info,
                tel,
                address,
                lng,
                lat,
                status
            })
            .save();
        this.success(shop);
    }
};
